<?php
namespace App\View;

use Cake\ORM\ResultSet;

trait SerializeTrait
{
    /**
     * Returns data to be serialized.
     *
     * @param array|string|bool $serialize The name(s) of the view variable(s) that
     *   need(s) to be serialized. If true all available view variables will be used.
     * @return array The data to serialize.
     */
    protected function _dataToSerialize($serialize = true)
    {
        if ($serialize === true) {
            $data = array_diff_key(
                $this->viewVars,
                array_flip($this->_specialVars)
            );

            return array_values($data);
        }

        if (is_array($serialize)) {
            $data = [];
            foreach ($serialize as $key) {
                if (array_key_exists($key, $this->viewVars)) {
                    $data[] = $this->viewVars[$key];
                }
            }

            return $data;
        }

        if (isset($this->viewVars[$serialize])) {
            $data = $this->viewVars[$serialize];

            if ($data instanceof ResultSet) {
                $data = iterator_to_array($data);
            }

            return is_array($data) ? $data : [$data];
        }

        return [];
    }
}
