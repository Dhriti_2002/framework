<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\View\View;
use Cake\View\Layout;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link https://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{
    /**
     * @param string|null $view
     * @param string|false|null $layout
     * @return
     */
    public function render(?string $view = null, $layout = null): string
    {
        if ($this->request->getParam('action') === 'view') {
            $vars = $this->viewVars;

            if (isset($vars['_serialize']) &&
                is_string($vars['_serialize']) &&
                isset($vars[$vars['_serialize']])) {
                $entity = $vars[$vars['_serialize']];

                if ($entity instanceof Entity) {
                    $repository = $entity->getSource();
                    $table = TableRegistry::get($repository);
                    $displayField = $table->getDisplayField();

                    if (!empty($displayField)) {
                        $title = $entity[$displayField] . ' - ' . $repository;
                        $this->assign('title', $title);
                    }
                }
            }
        }

        return parent::render($view, $layout);
    }
}
