<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Collections Controller
 *
 * @property \App\Model\Table\CollectionsTable $Collections
 *
 * @method \App\Model\Entity\Collection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CollectionsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('LinkedData');
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $collections = $this->paginate($this->Collections);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('collections', 'access_granted'));
        $this->set('_serialize', 'collections');
    }

    /**
     * View method
     *
     * @param string|null $id Collection id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $collection = $this->Collections->get($id, [
            'contain' => []
        ]);

        $artifacts = TableRegistry::get('artifacts_collections');
        $count = $artifacts->find('list', ['conditions' => ['collection_id' => $id]])->count();
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('collection', 'count', 'access_granted'));
        $this->set('_serialize', 'collection');
    }
}
