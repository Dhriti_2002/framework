<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Bibliography');
        $this->loadComponent('GeneralFunctions');

        $this->Auth->allow(['index', 'view', 'image']);
    }

    public function index($type)
    {
        if ($type == 'cdlp') {
            $this->paginate = [
                'order' =>[
                    'Articles.id' => 'ASC'
                ]
            ];
        } else {
            $this->paginate = [
                'order' =>[
                    'Articles.created' => 'DESC'
                ]
            ];
        };
        $this->paginate = [
            'contain' => ['Authors'],
        ];
        $articles = $this->paginate($this->Articles->find('type', ['type' => $type]));
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1,2]);

        $this->set(compact('articles', 'type', 'access_granted'));
        $this->set('_serialize', 'articles');
    }

    public function view($id)
    {
        $article = $this->Articles->get($id);
        $prefers = $this->RequestHandler->prefers();

        if ($prefers == 'pdf') {
            return $this->getResponse()
                ->withFile('webroot/pubs/' . $article->article_type . '/' . $article->pdf_link);
        } elseif ($prefers == 'html') {
            return $this->getResponse()
                ->withStringBody($article->content_html)
                ->withType('html');
        }

        $this->set('article', $article);
        $this->set('_serialize', 'article');
    }

    public function image($id, $name)
    {
        return $this->getResponse()->withFile('webroot/pubs/' . $id . '/images/' . $name);
    }
}
