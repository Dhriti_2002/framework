<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExternalResource Entity
 *
 * @property int $id
 * @property string|null $external_resource
 * @property string|null $base_url
 * @property string|null $project_url
 * @property string|null $abbrev
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class ExternalResource extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'external_resource' => true,
        'base_url' => true,
        'project_url' => true,
        'abbrev' => true,
        'artifacts' => true
    ];
}
