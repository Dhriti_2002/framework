<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Inscriptions Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\UpdateEventsTable|\Cake\ORM\Association\BelongsTo $UpdateEvents
 *
 * @method \App\Model\Entity\Inscription get($primaryKey, $options = [])
 * @method \App\Model\Entity\Inscription newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Inscription[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Inscription|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscription|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Inscription[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Inscription findOrCreate($search, callable $callback = null, $options = [])
 */
class InscriptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('inscriptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_events_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('atf')
            ->maxLength('atf', 4294967295)
            ->requirePresence('atf', 'create')
            ->notEmpty('atf');

        $validator
            ->scalar('jtf')
            ->maxLength('jtf', 4294967295)
            ->requirePresence('jtf', 'create')
            ->notEmpty('jtf');

        $validator
            ->scalar('transliteration')
            ->maxLength('transliteration', 4294967295)
            ->allowEmpty('transliteration');

        $validator
            ->scalar('transliteration_clean')
            ->maxLength('transliteration_clean', 4294967295)
            ->allowEmpty('transliteration_clean');

        $validator
            ->scalar('tranliteration_sign_names')
            ->maxLength('tranliteration_sign_names', 4294967295)
            ->allowEmpty('tranliteration_sign_names');

        $validator
            ->scalar('annotation')
            ->maxLength('annotation', 4294967295)
            ->allowEmpty('annotation');

        $validator
            ->boolean('is_atf2conll_diff_resolved')
            ->allowEmpty('is_atf2conll_diff_resolved');

        $validator
            ->scalar('comments')
            ->allowEmpty('comments');

        $validator
            ->scalar('structure')
            ->allowEmpty('structure');

        $validator
            ->scalar('translation')
            ->allowEmpty('translation');

        $validator
            ->scalar('transcription')
            ->allowEmpty('transcription');

        $validator
            ->nonNegativeInteger('accepted_by')
            ->requirePresence('accepted_by', 'create')
            ->notEmpty('accepted_by');

        $validator
            ->boolean('accepted')
            ->requirePresence('accepted', 'create')
            ->notEmpty('accepted');

        $validator
            ->scalar('inscription_comments')
            ->requirePresence('inscription_comments', 'create')
            ->notEmpty('inscription_comments');

        $validator
            ->boolean('is_latest')
            ->requirePresence('is_latest', 'create')
            ->notEmpty('is_latest');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));
        $rules->add($rules->existsIn(['update_events_id'], 'UpdateEvents'));

        return $rules;
    }
}
