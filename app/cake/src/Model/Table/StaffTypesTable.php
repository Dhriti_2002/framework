<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StaffTypes Model
 *
 * @property \App\Model\Table\StaffTable|\Cake\ORM\Association\HasMany $Staff
 *
 * @method \App\Model\Entity\StaffType get($primaryKey, $options = [])
 * @method \App\Model\Entity\StaffType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StaffType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StaffType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StaffType|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StaffType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StaffType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StaffType findOrCreate($search, callable $callback = null, $options = [])
 */
class StaffTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('staff_types');
        $this->setDisplayField('staff_type');
        $this->setPrimaryKey('id');

        $this->hasMany('Staff', [
            'foreignKey' => 'staff_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('staff_type')
            ->maxLength('staff_type', 200)
            ->requirePresence('staff_type', 'create')
            ->notEmpty('staff_type');

        return $validator;
    }
}
