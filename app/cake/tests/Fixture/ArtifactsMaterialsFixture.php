<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArtifactsMaterialsFixture
 *
 */
class ArtifactsMaterialsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'artifact_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'material_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'is_material_uncertain' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null],
        'material_color_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'material_aspect_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_artifacts_idx_9' => ['type' => 'index', 'columns' => ['artifact_id'], 'length' => []],
            'fk_materials_idx_2' => ['type' => 'index', 'columns' => ['material_id'], 'length' => []],
            'fk_artifacts_materials_material_color_id_idx' => ['type' => 'index', 'columns' => ['material_color_id'], 'length' => []],
            'fk_artifacts_materials_material_aspect_id_idx' => ['type' => 'index', 'columns' => ['material_aspect_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'fk_artifacts_9' => ['type' => 'foreign', 'columns' => ['artifact_id'], 'references' => ['artifacts', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_artifacts_materials_material_aspect_id' => ['type' => 'foreign', 'columns' => ['material_aspect_id'], 'references' => ['material_aspects', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_artifacts_materials_material_color_id' => ['type' => 'foreign', 'columns' => ['material_color_id'], 'references' => ['material_colors', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_materials_2' => ['type' => 'foreign', 'columns' => ['material_id'], 'references' => ['materials', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'artifact_id' => 1,
                'material_id' => 1,
                'is_material_uncertain' => 1,
                'material_color_id' => 1,
                'material_aspect_id' => 1
            ],
        ];
        parent::init();
    }
}
