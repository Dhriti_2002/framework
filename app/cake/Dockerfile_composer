FROM alpine:3.13
ARG version
ARG apk_repo
ARG php_version

LABEL description="Alpine-based php-fpm image for CDLI Cake"
LABEL maintainer="Cuneiform Digital Library Initiative <cdli.systems@gmail.com>"
LABEL version="$version"

ENV PHP_VERSION=$php_version

RUN set -ex; \
  addgroup -g 2354 -S cdli; \
  adduser -u 2354 -D -S -G cdli cdli;

RUN apk update;

RUN apk add --update \
  curl \
  libcurl \
  ca-certificates \
  php8-fpm \
  php8-ctype \
  php8-curl \
  php8-intl \
  php8-json \
  php8-mbstring \
  php8-mysqli \
  php8-mysqlnd \
  php8-opcache \
  php8-pecl-redis \
  php8-pecl-apcu \
  php8-pdo \
  php8-pdo_mysql \
  php8-session \
  php8-pdo_sqlite \
  php8-xml;

RUN apk add --update \
  git \
  nodejs \
  npm \
  php8 \
  php8-dom \
  php8-phar \
  php8-simplexml \
  php8-tokenizer \
  php8-xmlwriter \
  ruby-json \
  ruby-dev \
  ruby;

RUN rm -rf /var/cache/apk/* /tmp/pear ~/.pearrc;

# Installing python3 for python unittests.
RUN apk add --no-cache --virtual python3-dev && \
  apk add --no-cache --update python3 py3-pip && \
  pip3 install --upgrade pip setuptools;

# Installing scss-lint.
RUN npm install -g csslint; \
  gem update --system; \
  gem install rdoc --no-document; \
  gem install scss-lint;

# Sane defaults
RUN set -ex; \
  sed -ri \
    -e 's|^;?expose_php = .*|expose_php = off|' \
    -e 's|^;?allow_url_fopen = .*|allow_url_fopen = on|' \
    -e 's|^;?opcache.error_log=.*|opcache.error_log = /proc/self/fd/2|' \
    -e 's|^;?opcache.fast_shutdown=.*|opcache.fast_shutdown = 1|' \
    /etc/php8/php.ini;

# Installing PHPlint
RUN curl -L https://cs.symfony.com/download/php-cs-fixer-v2.phar -o php-cs-fixer;
RUN chmod a+x php-cs-fixer;
RUN mv php-cs-fixer /usr/local/bin/php-cs-fixer;rm -f php-cs-fixer;

# Installing composer
RUN php8 -r "copy('https://getcomposer.org/installer', 'composer-setup.php');";
RUN php8 composer-setup.php --install-dir=/usr/local/bin --filename=composer;
RUN rm -f composer-setup.php;

# Adding path for cake folder
RUN ln -s /usr/bin/php8 /usr/bin/php
ENV PATH=${PATH}:/srv/app/cake/bin:/srv/app/cake/vendor/bin
