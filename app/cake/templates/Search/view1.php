<?php if(!empty($result)){?>
<?php foreach ($result as $row) {?>
<table>
    <tr>
        <th rowspan="10"> 
            <?php echo $row["id"];?>
            <?=
                $this->Html->link(
                'Showmore',
                'showmore?CDLI='.$row["id"].'&Publication='.$row["_matchingData"]["Publications"]["id"] ,
                ['class' => 'btn btn-primary btn-lg active"', 'target' => '_blank']
                )
                ?>
        </th>
        <th > 
            <?php echo $row["_matchingData"]["Publications"]["designation"];?>
        </th>
    </tr>
    <tr>
        <td >
            <?php echo "Author: ".$row["_matchingData"]["Authors"]["author"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Publications Date: ".$row["_matchingData"]["Publications"]["year"]; ?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "CDLI No.: ".$row["id"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Collection: ".$row["_matchingData"]["Collections"]["collection"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Provenience: ".$row["_matchingData"]["Proveniences"]["provenience"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Period: ".$row["_matchingData"]["Periods"]["period"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Object Type: ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Material: ".$row["_matchingData"]["Materials"]["material"];?>
        </td>
    </tr>
    <tr>
        <td >
            <?php echo "Transliteration/Translation: ".$row["_matchingData"]["Inscriptions"]["transliteration"];?>
        </td>
    </tr>
</table>
<?php }?>
<?php }?>
