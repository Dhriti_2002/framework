<?php
/**
* CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
* @link          https://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       https://opensource.org/licenses/mit-license.php MIT License
*/

// Get  Roles of Authenticated User
$roles = $this->getRequest()->getSession()->read('Auth.User.roles');
$name = $this->getRequest()->getSession()->read('Auth.User.username');

// Check if role_id 1 or 2 is present
$ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2]) : [];

$ifRoleExists = !empty($ifRoleExists) ? 1 : 0;
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= h($this->fetch('title')) ?> - Cuneiform Digital Library Initiative
	</title>

	<?php if ($this->fetch('image')): ?>
		<meta name="twitter:card" content="summary_large_image">
		<meta property="og:image" content="<?= h($this->fetch('image')) ?>" />
	<?php else: ?>
		<meta name="twitter:card" content="summary">
	<?php endif; ?>

	<meta property="og:title" content="<?= h($this->fetch('title')) ?>" />
	<meta property="og:url" content="https://cdli.ucla.edu<?= $this->getRequest()->getRequestTarget() ?>" />
	<meta property="og:site_name" content="CDLI">
	<meta name="twitter:site" content="@cdli_news">

	<?php if ($this->fetch('description')): ?>
		<meta name="description" content="<?= h($this->fetch('description')) ?>">
		<meta property="og:description" content="<?= h($this->fetch('description')) ?>" />
	<?php endif; ?>

	<?= $this->Html->meta('icon') ?>
	<?= $this->Html->css('main.css')?>
	<?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>
	<?php
	// For using Bootstrap dropdowns, set variable $includePopper in your view
	if (isset($includePopper) && $includePopper) {
		echo $this->Html->script('popper.min.js');
	}
	?>

    <?= $this->Html->script('popper.min.js');?>
	<?= $this->Html->script('jquery.min.js')?>
	<?= $this->Html->script('bootstrap.min.js')?>
	<?= $this->Html->script('js.js')?>
	<?= $this->Html->script('drawer.js', ['defer' => true]) ?>
    <?= $this->element('google-analytics'); ?>
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>

<body>

	<!-- White translucent film  -->
		<div class="translucent-film d-none" id="faded-bg"></div>
	<!-- End White translucent film  -->

	<!-- No script navbar -->
		<noscript>
			<nav class="navbar navbar-expand-sm bg-light no-js-nav d-lg-none">
				<!-- Links -->
				<ul class="navbar-nav flex-row justify-content-around w-100">
					<li class="nav-item">
					<a class="nav-link mx-2" href="/browse">Browse</a>
					</li>
					<li class="nav-item">
					<a class="nav-link mx-2" href="/">Search</a>
					</li>
					<li class="nav-item">
					<a class="nav-link mx-2" href="#">About</a>
					</li>
					<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<li class="nav-item mx-2">
							<?= $this->Html->link("Login", '/login', ['class' => 'nav-link']) ?>
						</li>
						<li class="nav-item mx-2">
							<?= $this->Html->link("Register", '/register', ['class' => 'nav-link']) ?>
						</li>
					<?php } else { ?>
						<li class="nav-item mx-2">
							<?= $this->Html->link("Profile", ['controller' => 'Users'], ['class' => 'nav-link']) ?>
						</li>
						<li class="nav-item mx-2">
							<?= $this->Html->link("Logout", ['controller' => 'Logout'], ['class' => 'nav-link']) ?>
						</li>
					<?php } ?>
				</ul>
				<style>
				.fa-bars{
					display: none !important;
				}
				</style>
			</nav>
		</noscript>
	<!-- end No script navbar -->

	<header>
		<div class="bg-foggy-grey navbar navbar-expand d-none d-lg-block">
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
				<?php if ($ifRoleExists) { ?>
                    <li class="nav-item mx-3">
                        <?= $this->Html->link(in_array(1, $this->getRequest()->getSession()->read('Auth.User.roles')) ? "Admin Dashboard" : "Dashboard", '/admin/dashboard', ['class' => 'nav-link']) ?>
                    </li>
                <?php } ?>

					<li class="publication-nav nav-item mx-3 dropdown">
					<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Publications
					</a>
					<div class="dropdown-menu" aria-labelledby="">
						<a class="dropdown-item" href="/articles/cdlj"> Cuneiform Digital Library Journal</a>
						<a class="dropdown-item" href="/articles/cdlb"> Cuneiform Digital Library Bulletin</a>
                        <a class="dropdown-item" href="/articles/cdln">Cuneiform Digital Library Notes</a>
                        <a class="dropdown-item" href="/articles/cdlp">Cuneiform Digital Library Preprint</a>
					</div>
					
				</li>

					<li class="nav-item mx-3">
						<a class="nav-link" href="#">Resources</a>
					</li>


				<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<li class="nav-item mx-3">
							<?= $this->Html->link("Login", '/login', ['class' => 'nav-link']) ?>
						</li>
						<li class="nav-item mx-3 pr-5">
							<?= $this->Html->link("Register", '/register', ['class' => 'nav-link']) ?>
						</li>
					<?php } else { ?>
						<li class="publication-nav nav-item mx-3 dropdown">
					<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $name ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                       <?= $this->Html->link("My Profile", [
                        'controller' => 'Users',
                        'action' => 'profile',
                        'prefix' => false
                        ], [
                        'class' => 'dropdown-item'
                        ]) ?>
						   <?php if ($ifRoleExists) { ?>
						<a class="dropdown-item" href="/admin/artifacts"> Manage Artifacts</a>
                        <a class="dropdown-item" href="/admin/articles/cdlb">Manage Authors</a>
                        <a class="dropdown-item" href="/admin/publications">Manage Publications</a>
						<a class="dropdown-item" href="/admin/articles/cdlj">Manage CDL Journals</a>
						<?php } ?>
                        <hr>
                        <?= $this->Html-> link('Logout', [
                            'controller' => 'Logout',
                            'action' => 'index',
                            'prefix' => false
                        ], [
                            'class' => 'dropdown-item'
                            ]
                        ); ?>
					</div>
				</li>
					<?php } ?>
				</ul>
			</div>
		</div>

		<nav class="header navbar navbar-expand-lg bg-transparent py-0" id="navbar-main">
			<div class="container-fluid d-flex align-items-center justify-content-between my-5">
				<a class="navbar-brand logo" href="/">
					<div class="navbar-logo">
					<img src="/images/cdlilogo.svg" class="d-none d-md-block cdlilogo" alt="cdli-logo" />
						<img src="/images/logo-no-text.svg" class="d-md-none" alt="cdli-logo" />
					</div>
				</a>

				<div class="collapse navbar-collapse" id="navbarText">
					<ul class="navbar-nav ml-auto">

						<li class="nav-item mx-3 ">
							<?= $this->Html->link("Browse", '/browse', ['class' => 'nav-link']) ?>
						</li>
						
						<li class="nav-item mx-3">
							<a class="nav-link" href="/contribute">Contribute</a>
						</li>
						<li class="nav-item mx-3">
							<a class="nav-link" href="#">About</a>
						</li>
						<li class="nav-item mx-3 mr-5 dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search </a>
							<div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="/">Search</a>
								<a class="dropdown-item" href="/advancedsearch">Advanced search</a>
								<a class="dropdown-item" href="/SearchSettings">Search settings</a>
								<a class="dropdown-item" href="/cqp4rdf">Corpus search</a>
							</div>
						</li>
					</ul>
				</div>

				<a class="d-sm-block d-lg-none bg-transparent" onclick="openSlideMenu()">
					<span class="fa fa-bars fa-2x"></span>
				</a>
<!-- Start of Side Menu bar for small devices -->
				<aside id="side-menu" class="side-nav shadow">
					<button class="sidebar-btn-close px-3 border-0" onclick="closeSlideMenu()">
						×
					</button>
					<a href="/browse" class="mt-5">Browse</a>
					<a href="/contribute">Contribute</a>
					<a href="#">About</a>
					<a href="#">Resources</a>
					<?php if ($ifRoleExists) { ?>
						<?= $this->Html->link(in_array(1, $this->getRequest()->getSession()->read('Auth.User.roles')) ? "Admin Dashboard" : "Dashboard", '/admin/dashboard', ['class' => 'nav-link']) ?>
					<?php } ?>
					<a  data-toggle="collapse" href="#collapseExampleSearch" role="button" aria-expanded="false" aria-controls="collapseExampleSearch" class="border-0 mb-1">
						Search <span id="drawer-arrowSearch" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleSearch">
						<a class="ml-5" href="/">Search</a>
						<a class="ml-5" href="/advancedsearch">Advanced search</a>
						<a class="ml-5" href="/SearchSettings">Search settings</a>
						<a class="ml-5" href="/cqp4rdf">Corpus search</a>
					</div>

					<a  data-toggle="collapse" href="#collapseExamplePublication" role="button" aria-expanded="false" aria-controls="collapseExamplePublication" class="border-0 mb-1">
						Publications<span id="drawer-arrowPublication" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExamplePublication">
							<a class="dropdown-item" href="/articles/cdlj">CDLJ</a>
							<a class="dropdown-item" href="/articles/cdlb">CDLB</a>
							<a class="dropdown-item" href="/articles/cdln">CDLN</a>
							<a class="dropdown-item" href="/articles/cdlp">CDLP</a>
					</div>

					

					<?php if (!$this->getRequest()->getSession()->read('Auth.User')) { ?>
						<a class="border-0 mb-1" href="/login">Login</a>
						<a class="border-0 mb-1" href="/register">Register</a>
					<?php } else { ?>
					<a  data-toggle="collapse" href="#collapseExampleProfile" role="button" aria-expanded="false" aria-controls="collapseExampleProfile" class="border-0 mb-1">
						<?php echo $name ?><span id="drawer-arrowProfile" class="fa fa-angle-down float-right" aria-hidden="true"></span>
					</a>
					<div class="collapse embedded-links" id="collapseExampleProfile">
						<?= $this->Html->link("My Profile", [
							'controller' => 'users',
							'action' => 'view',
							$this->getRequest()->getSession()->read('Auth.User.username')
						]) ?>

						<?php if ($ifRoleExists) { ?>
							<a href="/admin/artifacts"> Manage Artifacts</a>
							<a href="/admin/articles/cdlb">Manage Authors</a>
							<a href="/admin/publications">Manage Publications</a>
							<a href="/admin/articles/cdlj">Manage CDL Journals</a>

						<?php } ?>
						<?= $this->Html->link(
							'Logout',
							[
								'controller' => 'Logout',
								'action' => 'index',
								'prefix' => false
							],
							[
								'class' => 'dropdown-item'
							]
						); ?>


					</div>
					<?php }?>
				</aside>
<!-- End of Side Menu bar for small devices -->


			</div>
		</nav>
	</header>

	<div class="container-fluid text-center contentWrapper">
		<?= $this->Flash->render() ?>
		<?= $this->fetch('content') ?>
	</div>

	<noscript>
		<p class="alert alert-danger alert-dismissible fade show textcenter">Your browser does not support javascript please enable it for better experince!</p>
	</noscript>


	<footer>
		<div class="container">
			<div>
				<div class="row footer-1 py-5">
					<div class="col-lg-3 d-none d-lg-block">
						<h2 class="heading">Navigate</h2>
						<p><a href="/browse">Browse collection</a></p>
						<p><a href="/contribute">Contribute</a></p>
						<p><a href="#">About CDLI</a></p>
						<p><a href="#">Search collection</a></p>
					</div>
					<div class="col-md-6 col-lg-4">
						<h2 class="heading">Acknowledgement</h2>
						<p class="backers">
                            Support for this initiative has been generously provided by the
							<a href="https://mellon.org/" target="_blank">Mellon Foundation</a>,
                            the <a href="https://www.nsf.gov/" target="_blank">NSF</a>,
							the <a href="https://www.neh.gov/" target="_blank">NEH</a>,
							the <a href="https://www.imls.gov/" target="_blank">IMLS</a>,
							the <a href="https://www.mpg.de/en" target="_blank">MPS</a>,
							<a href="http://www.ox.ac.uk/" target="_blank">Oxford University</a>
							and <a href="http://www.ucla.edu/" target="_blank">UCLA</a>,
							with additional support
                            from <a href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx" target="_blank">SSHRC</a> and
							the <a href="https://www.dfg.de/" target="_blank">DFG</a>.
							Computational resources and network services are provided by
							<a href="https://humtech.ucla.edu/" target="_blank">UCLA’s HumTech</a>,
                            <a href="https://www.mpiwg-berlin.mpg.de/" target="_blank">MPIWG</a>,
							and <a href="https://www.computecanada.ca/" target="_blank">Compute Canada</a>.
                        </p>
					</div>
                    <div class="col-lg-1 d-none d-lg-flex"></div>
					<div class="col-md-6 col-lg-4 contact">
						<h2 class="heading">Contact Us</h2>
						<p class="p">
							<li style="list-style-type : none">Cuneiform Digital Library Initiative</li>
							<li style="list-style-type : none">Linton Rd, Oxford OX2 6UD</li>
							<li style="list-style-type : none">United Kingdom</li>
						</p>
						<div class="d-flex">
							<div class="twitter">
								<a href="https://twitter.com/cdli_news" target="_blank" aria-label="twitter-page">
									<span class="fa fa-twitter" aria-hidden="true"></span>
								</a>
							</div>
							<div class="mail">
								<a href="mailto:cdli@ucla.edu" target="_blank" aria-label="mail-to-cdli">
									<span class="fa fa-envelope fa-3x" aria-hidden="true"></span>
								</a>
							</div>
							<div> <a href="https://opencollective.com/cdli/donate" class="btn donate" role="button" target="_blank">Donate</a></div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="footer-end p-4">
	<div class="text-center text-white">
		© 2019-2020 Cuneiform Digital Library Initiative.
	</div>
</div>
</footer>
</body>
</html>
