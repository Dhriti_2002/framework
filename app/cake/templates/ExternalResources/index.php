<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource[]|\Cake\Collection\CollectionInterface $externalResources
 */
?>


<h3 class="display-4 pt-3"><?= __('External Resources') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('external_resource') ?></th>
            <th scope="col"><?= $this->Paginator->sort('base_url') ?></th>
            <th scope="col"><?= $this->Paginator->sort('project_url') ?></th>
            <th scope="col"><?= $this->Paginator->sort('abbrev') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($externalResources as $externalResource): ?>
        <tr align="left">
            <td>
                <a href="/externalResources/<?=h($externalResource->id)?>">
                <?= h($externalResource->external_resource) ?>
                </a>
            </td>
            <td><?= h($externalResource->base_url) ?></td>
            <td><?= h($externalResource->project_url) ?></td>
            <td><?= h($externalResource->abbrev) ?></td>
            <td class="btn">
            <?php if ($access_granted): ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $externalResource->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $externalResource->id], 
                        ['confirm' => __('Are you sure you want to delete # {0}?',  $externalResource->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

