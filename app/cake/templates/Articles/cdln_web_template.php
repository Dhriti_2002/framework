<html>
<head>
<title><?php echo $cdln['title']?></title>
<link rel="stylesheet" href="/assets/css/artilce_web_template.css"/> 
<style>
body{
	width:70%;
}
#CKAddedHeaderTable{
	display:none;
}
</style>

<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script type="text/javascript" async
  src="/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML">
</script>
</head>
<body>
<table width="750" border="0" cellpadding="0" cellspacing="5">
	<tr>
	    <td valign="top" width="400"> 
	    <p style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif; font-size:9pt">
	        Cuneiform Digital Library Journal <br>
	        <b> 2018:001 </b> 
	        <br>
	        <font size="1"> ISSN 1540-8779 <br>
	        &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp; 
	    </p>
	    </td>
	    <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#1461ab"> 
	    <p style="line-height:15.0pt; font-size:9pt;color:white;">
	        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> <a href="/"> CDLI Home </a> <br>
	        <a href=""> CDLI Publications </a> <br>
	        <a href="" target="link"> Editorial Notes </a> <br>
	        <a href="" target="link"> Abbreviations </a> <br>
	        <a href="" target="link"> Bibliography </a> <br>
	        <br>
	        <a href="" target="blank">PDF Version of this Article </a> 
	        <br>
	        <a href="" target="link"> Get Acrobat Reader </a> <br>
	        <a href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a> </font> 
	    </p>
	    </td>
	    <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
	</tr>
	<tr>
	    <td> 
	    <p>
	        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> 
	        <h2 style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
	        <br>
	        <p id="pArticleName"><?php echo $cdln['title']; ?></p>
	        </h2>
	        <b> <p id="pArticleAuthors"> <?php echo $cdln['authors']; ?> </p</b> <br>
	        <br>
	        <i>University</i> 
	        <br>
	        <br>
	        <b> Keywords </b> <br>
	        </font> 
	    </p>
	    </td>
	</tr>
	<tr>
	    <td colspan="3" align="center"> 
	    <hr align="center" width="600" size="2">
	    </td>
	</tr>
	<tr>
	    <td colspan="3" align="left"> 
	    <br>
	    <p>
	    <i><b>Abstract</b><br><br></i>
	    </p>
	    <div id="pArticleContent">
			<?php echo $cdln['content_html']?>
		</div>
	    
	    </td>
	</tr>
	<tr>
		<td colspan="3" align="center"> 
		<hr>
		<p>
			<font face="S<sup>ki</sup>a, Verdana, Arial, Helvetica, sans-serif"> <b> Version: 
<!-- InstanceBeginEditable name="version date"-->
			<font face="S<sup>ki</sup>a, Verdana, Arial, Helvetica, sans-serif"> <b><?php echo $cdln['modified']?></b> &nbsp; </font> 
<!-- InstanceEndEditable-->
			</b> </font> 
		</p>
		</td>
	</tr>
</table>
</body>
</html>
