<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive $archive
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($archive) ?>
            <legend class="capital-heading"><?= __('Add Archive') ?></legend>
            <?php
                echo $this->Form->control('archive');
                echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
