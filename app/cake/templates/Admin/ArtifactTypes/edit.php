<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType $artifactType
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactType) ?>
            <legend class="capital-heading"><?= __('Edit Artifact Type') ?></legend>
            <?php
                echo $this->Form->control('artifact_type');
                echo $this->Form->control('parent_id', ['options' => $parentArtifactTypes, 'empty' => true]);
            ?>
<div>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactType->id],
                ['class' => 'btn btn-danger float-right',
                'confirm' => __('Are you sure you want to delete # {0}?', $artifactType->id)]
            )
        ?>
        </div>
    </div>

</div>
