<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="abbreviations form content">
            <?= $this->Form->create($abbreviation) ?>
            <fieldset>
                <legend><?= __('Edit Abbreviation') ?></legend>
                <?php
                    echo $this->Form->control('abbreviation');
                    echo $this->Form->control('fullform');
                    echo $this->Form->control('publication_id', ['options' => $publications, 'empty' => true]);
                    echo $this->Form->control('type');
                ?>
            </fieldset>
            <div>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
            <?= $this->Form->end() ?>
            <?= $this->Form->postLink(
                __('Delete Abbreviation'),
                ['action' => 'delete', $abbreviation->id],
                ['class' => 'btn btn-danger float-right',
                'confirm' => __('Are you sure you want to delete # {0}?', $abbreviation->id)]
            )
        ?>
        </div>
        </div>
    </div>

</div>
