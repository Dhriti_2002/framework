<div class="container">

    <h1 class="display-3 text-left header-txt">CDLI tablet</h1>
    <p class="text-left home-desc mt-4">Web admin interface for data entry and management.</p>
    
    <div class="card">
        <div class="card-body">
            <!-- Back and Add buttons -->
            <table>
                <form class="form-inline">
                    <tbody>
                        <td>
                            <?php echo $this->Html->link(
                                '<< Back', 
                                array('controller'=>'CdliTablet', 'action'=>'index'), 
                                array('class'=>'btn btn-dark'));
                            ?>
                        </td>
                    </tbody>
                </form>
            </table>
            <div class="row justify-content-md-center">
                <div class="col-lg-7 boxed">
                    <legend class="capital-heading row justify-content-md-center"><?= __('Submit an article for a specific calendar date') ?></legend>

                    <?php echo $this->Form->create(null, ['enctype'=>'multipart/form-data']); ?>

                    <form class="needs-validation" method="post" accept-charset="utf-8" role="form" action="#" nonvalidate>
                    <table cellpadding="10" cellspacing="10">
                        <div style="display:none;">
                            <input type="hidden" name="_method" class="form-control" value="POST" />
                        </div>
                        <tr>
                            <div class="form-group row form-group date">
                                <label for="displaydate" class="col-sm-3 col-form-label control-label">Date (required)</label>
                                <div class="col-sm-10">
                                    <input type="date" name="displaydate" class="form-control" id="displaydate" required />
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row form-group text">
                                <label for="theme" class="col-sm-3 col-form-label control-label">Theme (optional)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="theme" class="form-control" id="theme" placeholder="Enter theme" />
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row form-group text">
                                <label for="shorttitle" class="col-sm-3 col-form-label control-label">Short Title (required)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="shorttitle" class="form-control " id="shorttitle" placeholder="Enter short title" required />
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row form-group text">
                                <label for="longtitle" class="col-sm-3 col-form-label control-label">Long Title (optional)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="longtitle" class="form-control " id="longtitle" placeholder="Enter long title" />
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row form-group text">
                                <label for="shortdesc" class="col-sm-3 col-form-label control-label">Short Description (required)</label>
                                <div class="col-sm-10">
                                    <textarea type="text" name="shortdesc" class="form-control" id="shortdesc" rows="2" maxlength="4000" placeholder="Enter short description" required ></textarea>
                                </div>
                                <div class="col-sm-10">
                                    <small id="shortdesc" class="form-text text-muted">Description should be from 10-50 words [UTF8 & HTML are supported].</small>
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row form-group text">
                                <label for="longdesc" class="col-sm-3 col-form-label control-label">Long Description (required)</label>
                                <div class="col-sm-10">
                                    <textarea type="text" name="longdesc" class="form-control" id="longdesc" rows="2" maxlength="10000" placeholder="Enter long description" required ></textarea>
                                </div>
                                <div class="col-sm-10">
                                    <small id="longdesc" class="form-text text-muted">Description should be from 100-400 words [UTF8 & HTML are supported].</small>
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row form-group text">
                                <label for="createdby" class="col-sm-3 col-form-label control-label">Created by (required)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="createdby" class="form-control " id="createdby" placeholder="Enter creator name" required />
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <div class="form-group row">
                                <label for="file" class="col-sm-3 col-form-label control-label">Image (required)</label>
                                <div class="col-sm-10">
                                    <input type="file" name="file" class="form-control" id="file" required />
                                </div>
                                <div class="col-sm-10">
                                    <small id="file" class="form-text text-muted">Select an image to upload. Choose a 2048 x 1536 JPEG file.</small>
                                </div>
                            </div>
                        </tr>
                </table>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Save">
        </div>
     </form>
     <?php 
        echo $this->Form->end() 
    ?>
</div>
</div>
</div>
</div>
</div>
