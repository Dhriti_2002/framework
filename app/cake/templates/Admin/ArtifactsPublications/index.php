<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication[]|\Cake\Collection\CollectionInterface $artifactsPublications
 */
?>
<h1 class="display-3 header-text text-left"><?= __('Artifact-Publication Links') ?></h1>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('exact_reference') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_comments') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsPublications as $artifactsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsPublication->id) ?></td> -->
            <td><?= $this->Html->link($artifactsPublication->artifact_id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsPublication->artifact_id]) ?></td>
            <td><?= h($artifactsPublication->artifact->designation) ?></td>
            <td><?= $this->Html->link($artifactsPublication->publication_id, ['controller' => 'Publications', 'action' => 'view', $artifactsPublication->publication_id]) ?></td>
            <td><?= h($artifactsPublication->publication->designation) ?></td>
            <td><?= h($artifactsPublication->exact_reference) ?></td>
            <td><?= h($artifactsPublication->publication_type) ?></td>
            <td><?= h($artifactsPublication->publication_comments) ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsPublication->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsPublication->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsPublication->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
             
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

