<?php
/*  PHP 5.6+ Secrets Library -- Version 0.9.0
 *
 *  USAGE:
 *    Secrets::getSecrets('secretID', ...) => returns associative array
 *    Secrets::getLocalSecrets('secretID', ...) => returns associative array
 *    Secrets::getRemoteSecrets('secretID', ...) => returns associative array
 *
 */


final class Secrets {
  private static $_INITIALIZED = false;
  private static $_SECRET_SRC = "local";
  private static $_SECRETS_URI = "http://localhost/secrets?";
  private static $_SECRETS_LOCAL = "/var/secrets/*.json";


  // Get an associative array of secrets from somewhere.
  public static function getSecrets(...$ids) {
    if (self::$_SECRET_SRC === "remote") {
      return self::getRemoteSecrets(...$ids);
    } else {
      return self::getLocalSecrets(...$ids);
    }
  }


  // Get local secrets.
  public static function getLocalSecrets(...$ids) {
    $secrets = array();
    $results = array();
    $secretFiles = glob(self::$_SECRETS_LOCAL);
    foreach ($secretFiles as $secretFile) {
      $data = file_get_contents($secretFile);
      if ($data === false) {
        $results["_error"] = "file read error";
        continue;
      }
      $json = json_decode($data, true);
      if (!is_array($json)) {
        $results["_error"] = "JSON error";
        continue;
      }
      $secrets = array_merge($secrets, $json);
    }
    foreach ($ids as $id) {
      $results[$id] = (isset($secrets[$id]) ? $secrets[$id] :
                       array("_error" => "not found"));
    }
    return $results;
  }


  // Get secrets from an HTTP endpoint like SecretsD.
  public static function getRemoteSecrets(...$ids) {
    $uri = self::$_SECRETS_URI . self::_parseIdsToQuery(...$ids);
    $curlHandler = curl_init($uri);
    curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlHandler, CURLOPT_FAILONERROR, 1);
    $res = curl_exec($curlHandler);
    if (curl_errno($curlHandler)) {
      $out = array("_error" => curl_error($curlHandler));
      curl_close($curlHandler);
      return $out;
    }
    curl_close($curlHandler);
    $json = json_decode($res, true);
    return ($json !== null) ? $json : array("_error" => "JSON parse error");
  }


  // Parse a sequence of IDs into a query string.
  private static function _parseIdsToQuery(...$ids) {
    $queryArr = array_map("urlencode", $ids);
    $queryStr = implode("&", $queryArr);
    return $queryStr;
  }


  // Determine environment and secret source on first require.
  public static function InitClass() {
    if (self::$_INITIALIZED) { return; }
    if (preg_match('/\b(?:prod|staging|remote)/i', getenv("CDLI_ENV")) === 1) {
      self::$_SECRET_SRC = "remote";
    } else {
      self::$_SECRET_SRC = "local";
    }
    self::$_SECRETS_URI = (getenv("SECRETS_URI") ?: self::$_SECRETS_URI);
    self::$_SECRETS_LOCAL = (getenv("SECRETS_LOCAL") ?: self::$_SECRETS_LOCAL);
    self::$_INITIALIZED = true;
  }


  // Don't instantiate this class.
  private function __clone() {}
  private function __construct() {}
}

Secrets::InitClass();

?>
