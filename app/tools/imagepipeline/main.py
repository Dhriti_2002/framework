import redis
from redis import RedisError
import json
from minio.error import ResponseError
from minio import Minio
import threading
from worker import Worker
import os

try:
    # intanticate redis
    r = redis.Redis(host="redis", port=6379, db=9, decode_responses=True)

    # Instantiate a client
    mcclient = Minio('minio:9000',
                    access_key=os.environ['MINIO_ACCESS_KEY'],
                    secret_key=os.environ['MINIO_SECRET_KEY'], secure=False)
except Exception as e:
    print("Failed to initalize Redis or Minio")
    print(e)

def addToThread(worker):
        thread = threading.Thread(target=worker.execute, args=())
        thread.daemon = True
        thread.start()

print('starting worker')

taskcount = 0
queue = 'image'


r.ping()
queues = r.smembers('cdli:queues')
print('working on : ', queues)
image = Worker(queue,r, mcclient)
addToThread(image)
while True:
    job = r.blpop('cdli:queue:'+queue)
    paykey = 'cdli:job:'+job[1]
    payload = r.hgetall(paykey)
    image.addJob(payload['data'], json.loads(payload['storage']), payload['function'], paykey)
    taskcount = taskcount+1
    # print(payload[b'session'])