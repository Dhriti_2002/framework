import numpy as np
import cv2 as cv2
import os


class Imgclass:

    def __init__(self, img):
        try:
            self.img = cv2.imread(img)
        except Exception as e:
            print("Error reading file")
            print(e)
        self.img_o = img
        self.name = os.path.basename(img)
        self.img_allcontour = None
        self.alpha_img = None
        self.img_blur_guass = None
        self.img_blur_med = None
        self.img_contour = None
        self.img_contours = None
        self.img_eroded = None
        self.img_grey = None
        self.img_largest_contour_index = None
        self.img_mask = None
        self.img_masked = None
        self.img_open = None
        self.img_png = None
        self.img_thresh = None
        self.img_alpha = None

    def __del__(self):
        del self.img
        del self.img_allcontour
        del self.alpha_img
        del self.img_o
        del self.img_blur_guass
        del self.img_blur_med
        del self.img_contour
        del self.img_contours
        del self.img_eroded
        del self.img_grey
        del self.img_largest_contour_index
        del self.img_mask
        del self.img_masked
        del self.img_open
        del self.img_png
        del self.img_thresh
        del self.img_alpha

    def boost(self, img):
        self.img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
        channel = cv2.split(self.img_hsv)
        channel[2] = cv2.equalizeHist(channel[2])
        img_boosted_hsv = cv2.merge(channel, self.img_hsv)
        self.img_boosted_rgb = cv2.cvtColor(img_boosted_hsv, cv2.COLOR_HSV2RGB)
        return self.img_boosted_rgb

    def alpha(self, img, alpha=5, beta=90):
        self.img_alpha = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)
        return self.img_alpha

    def medianblur(self, img, factor=3):
        self.img_blur_med = cv2.medianBlur(img, factor)
        return self.img_blur_med

    def guassianblur(self, img, factor=0, kernel=(3, 3)):
        self.img_blur_guass = cv2.GaussianBlur(img, kernel, factor)
        return self.img_blur_guass

    def grey(self, img):
        self.img_grey = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        return self.img_grey

    def thresh(self, img):
        _, self.img_thresh = cv2.threshold(
            img, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        return self.img_thresh

    def opening(self, img, iterations=5):  # 5
        kernel = np.ones((2, 2), np.uint8)
        self.img_open = cv2.morphologyEx(
            img, cv2.MORPH_OPEN, kernel, iterations=iterations)
        return self.img_open

    def closing(self, img, iterations=20):  # 20
        kernel = np.ones((3, 3), np.uint8)
        self.img_open = cv2.morphologyEx(
            img, cv2.MORPH_CLOSE, kernel, iterations=iterations)
        return self.img_open

    def erode(self, img, iterations=10):  # 10
        kernel = np.ones((2, 2), np.uint8)
        self.img_eroded = cv2.erode(img, kernel, iterations=iterations)
        return self.img_eroded

    def contours(self, img):
        contours, hierarchy = cv2.findContours(
            img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
        self.img_contours = contours
        return self.img_contours

    def contourNotTouchesImageBorder(self, contour, img):

        x, y, w, h = cv2.boundingRect(contour)
        retval = True
        xMin = 0
        yMin = 0
        xMax = img.shape[1] - 1
        yMax = img.shape[0] - 1
        if(x <= xMin or
                y <= yMin or
                w >= xMax or
                h >= yMax):
                retval = False
        return retval

    def findbigcontour(self, img, contours):
        largest_area = 0
        largest_contour_index = 0
        self.img_largest_contour_index = 0
        for n, cnt in enumerate(contours):
            area = cv2.contourArea(cnt)
            if (area > largest_area):
                if self.contourNotTouchesImageBorder(cnt, img):
                    largest_area = area
                    largest_contour_index = n
                    self.img_largest_contour_index = largest_contour_index
        return self.img_largest_contour_index

    def addPadding(self, img):
        bordersize = int((img.shape[0] + img.shape[1]) * (2/100))
        return cv2.copyMakeBorder(
            img,
            top=bordersize,
            bottom=bordersize,
            left=bordersize,
            right=bordersize,
            borderType=cv2.BORDER_CONSTANT,
            value=[0, 0, 0]
        )

    def correctAlignment(self, img):
        # rect = cv2.minAreaRect(self.img_contours[self.img_largest_contour_index])
        # center = rect[0]
        # size = rect[1]
        # angle = rect[2]
        # if angle > 0:
        #     angle = angle if angle % 90 > angle else angle % 90
        # else:
        #     angle = angle if angle % 90 < angle else angle % 90
        # center, size = tuple(map(int, center)), tuple(map(int, size))
        # height, width = img.shape[0], img.shape[1]
        # M = cv2.getRotationMatrix2D(center, angle, 1)
        # img_rot = cv2.warpAffine(img, M, (width, height))
        # return img_rot

        rect = cv2.minAreaRect(
            self.img_contours[self.img_largest_contour_index])
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        width = int(rect[1][0])
        height = int(rect[1][1])
        src_pts = box.astype("float32")
        dst_pts = np.array([[0, height-1],
                            [0, 0],
                            [width-1, 0],
                            [width-1, height-1]], dtype="float32")

        # the perspective transformation matrix
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)

        # directly warp the rotated rectangle to get the straightened rectangle
        warped = cv2.warpPerspective(self.img_masked, M, (width, height))
        padded = self.addPadding(warped)
        return padded

    def crop(self, cont, index, img):
        mask = np.zeros(img.shape, dtype=np.uint8)
        mask = cv2.fillPoly(img=img.copy(), pts=cont[index].reshape(
            1, -2, 2), color=(0, 0, 0))
        masked_image = cv2.bitwise_and(img, ~mask)
        self.img_masked = masked_image
        self.img_mask = mask
        self.img_contour = img.copy()
        self.img_allcontour = img.copy()
        cv2.drawContours(self.img_contour, cont, index, (0, 255, 0), 3)
        cv2.drawContours(self.img_allcontour,
                         self.img_contours, -1, (0, 255, 0), 3)
        return self.img_masked, self.img_mask, self.img_contour

    def cropPNG(self, cont, index, img):
        mask = np.zeros(img.shape, dtype=np.uint8)
        mask = cv2.fillPoly(img=img.copy(), pts=cont[index].reshape(
            1, -2, 2), color=(0, 0, 0))
        masked_image = cv2.bitwise_and(img, ~mask)
        self.img_png = cv2.cvtColor(masked_image, cv2.COLOR_BGR2BGRA)
        return self.img_png

    def addAlphaChannel(self, img):
        height, width, channels = img.shape
        if channels < 4:
            self.alpha_img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)
            return self.alpha_img
        return img

    def saveImage(self, img, location, name, compression=None):
        try:
            temp = os.getcwd()
            os.chdir(location)
            if compression:
                return cv2.imwrite(name, img, compression)
            else:
                return cv2.imwrite(name, img)
        except Exception as e:
            print("Failed to save file:", name)
            print(e)
        finally:
            os.chdir(temp)

    def run(self):
        ret = self.addAlphaChannel(self.img)

        ret = self.alpha(self.img)
        ret = self.medianblur(ret)
        grey = self.grey(ret)
        blur = self.guassianblur(grey, 5, (7, 7))
        thresh = self.thresh(blur)
        closed = self.closing(thresh)
        eroded = self.erode(closed)

        cont = self.contours(eroded)
        self.findbigcontour(ret, cont)

        cropPNG = self.cropPNG(
            cont, self.img_largest_contour_index, self.alpha_img)
        final = self.crop(cont, self.img_largest_contour_index, self.img)

        pngRot = self.correctAlignment(cropPNG)
        mainRot = self.correctAlignment(self.img_masked)
        self.saveImage(self.img_masked, '/tmp', self.name +
                       '.lossy.jpeg')  # Saving PNG File
        self.saveImage(mainRot, '/tmp', self.name +
                       '.processed.jpeg')  # Saving JPEG Export
        self.saveImage(self.img_contour, '/tmp', self.name +
                       '.contours.jpeg')  # Saving image with all contours
        self.saveImage(self.img_mask, '/tmp', self.name +
                       '.mask.jpeg')  # Saving the inverse of crop
        self.saveImage(cropPNG, '/tmp', self.name +
                       '.half.processed.png', [cv2.IMWRITE_PNG_COMPRESSION, 5])

        return [('/tmp/'+self.name+i, self.name+i) for i in ['.half.processed.png', '.contours.jpeg', '.mask.jpeg', '.lossy.jpeg', '.processed.jpeg']]


class Job:
    def __init__(self, x, y):
        print("Got till here")
        self.rawJobData = x
        self.minioObjectsCompleted = y[0]
        self.minioObjectsFailed = y[1]

    def perform(self):
        minioObjects = []
        count = 0
        for i in self.minioObjectsCompleted:
            tempJobObj = Imgclass(i)
            returnJob = tempJobObj.run()
            del tempJobObj
            for i in returnJob:
                count = count + 1
                minioObjects.append(('image-scratch', i[0], i[1]))
        return (count, minioObjects)
