# This setups the utils for GitlabCi

# 1. Install php7.3
apt-get install -y -qq php7.3

# 2. Install php7.3 extensions
apt-get install -y -qq php7.3-intl php7.3-cli php7.3-mbstring php7.3-common php7.3-xmlwriter php7.3-xml

# 3. Install ruby-dev
apt-get install -y -qq ruby-dev

# 4. Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# 5. Install Node & npm
apt-get install -y nodejs
apt-get install -y npm

# 6. Install CSSlint.
npm install -g csslint

# 7. Install SCSS_Lint
gem install scss_lint

# 8. Install phplint.
wget https://cs.symfony.com/download/php-cs-fixer-v2.phar -O php-cs-fixer

# 9. Setups for phplint.
chmod a+x php-cs-fixer
mv php-cs-fixer /usr/local/bin/php-cs-fixer

# Show composer version.
composer --version 

# Populate vendor files. 
(cd app/cake; composer install)

# Show node version.
node --version

# Show npm version.
npm --version
